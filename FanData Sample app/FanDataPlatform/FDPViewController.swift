//
//  FDPViewController.swift
//  FanData Sample app
//
//  Created by Anthony Soulier on 24/03/2021.
//

import UIKit
import PLFanData
import PLFanDataCommon

class FDPViewController: UIViewController {

    enum PlacementId: String {
        case inlineBannerDefaultTemplate = "inner-banner-default-template-placement-id"
        case inlineBannerTemplateTwo = "inner-banner-template-two-placement-id"
        case interstitialDefaultTemplate = "interstitial-default-template-placement-id"
        case popupDefaultTemplate = "popup-default-template-placement-id"
        case popupTemplateTwo = "popup-template-two-placement-id"
        case pinnedDefaultTemplate = "pinned-default-template-placement-id"
    }

    enum VariantId: String {
        case inlineBannerDefaultTemplate = "inner-banner-default-template-variant-id"
        case inlineBannerTemplateTwo = "inner-banner-template-two-variant-id"
        case interstitialDefaultTemplate = "interstitial-default-template-variant-id"
        case popupDefaultTemplate = "popup-default-template-variant-id"
        case popupTemplateTwo = "popup-template-two-variant-id"
        case pinnedDefaultTemplate = "pinned-default-template-variant-id"
    }

    enum Constant {
        static let iabGDPRAppliesKey = "IABTCF_gdprApplies"
    }

    private let tableView = UITableView()
    private lazy var consentBarButton = UIBarButtonItem(
        image: UIImage(systemName: "checkmark.shield"),
        style: .plain,
        target: self,
        action: #selector(toggleConsent)
    )
    
    private lazy var shareBarButton = UIBarButtonItem(
        image: UIImage(systemName: "square.and.arrow.up"),
        style: .plain,
        target: self,
        action: #selector(shareAction)
    )

    private var items: [Any] = (0...100).map { "\($0)" }

    private let widgetProvider: FDPWidgetProvider
    private let eventTracker: FDPEventTracker
    
    init(widgetProvider: FDPWidgetProvider, eventTracker: FDPEventTracker) {
        self.eventTracker = eventTracker
        self.widgetProvider = widgetProvider
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupTableView()
        updateHasConsentIcon()
        
        navigationItem.leftBarButtonItem = consentBarButton
        navigationItem.rightBarButtonItem = shareBarButton

        // For Placement Id
        fetchWidget(for: PlacementId.inlineBannerDefaultTemplate.rawValue)

        // For Variant Id
        // fetchWidget(variantId: VariantId.inlineBannerDefaultTemplate.rawValue)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Track a screen view event
        eventTracker.trackScreenViewEvent(screenName: "FDPViewController")
    }
    
    @objc private func shareAction() {
        eventTracker.trackShareEvent(payload: ["url" : "www.my-shared-url.com"])
    }
    
    @objc private func toggleConsent() {
        
        /// This should never be done in production
        let gdprApplies = UserDefaults.standard.string(forKey: Constant.iabGDPRAppliesKey) ?? "1"
        let toggled = gdprApplies == "1" ? "0" : "1"
        UserDefaults.standard.setValue(toggled, forKey: Constant.iabGDPRAppliesKey)
        
        updateHasConsentIcon()
    }
    
    private func updateHasConsentIcon() {
        let applies = UserDefaults.standard.string(forKey: Constant.iabGDPRAppliesKey) ?? "1" == "1"
        let imageName = applies ? "xmark.shield" : "checkmark.shield"
        consentBarButton.image = UIImage(systemName: imageName)
    }
    
    private func setupTableView() {
        
        // Register a FDPWidgetCellContainer, so you can use it to display a widget
        tableView.register(FDPWidgetTableViewCell.self, forCellReuseIdentifier: "FDPWidgetContainerCell")
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.dataSource = self
                
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DummyDataCell")
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        ])
    }
    
    private func fetchWidget(for placementId: String) {
        widgetProvider.widget(placementId: placementId) { [weak self] (result) in
            guard let self = self else { return }
            try? self.handleWidgetRender(result: result, at: 5)
        }
    }

    private func fetchWidget(variantId: String) {
        widgetProvider.widget(variantId: variantId) { [weak self] result in
            guard let self = self else { return }
            try? self.handleWidgetTypeRender(result: result, at: 5)
        }
    }

    private func handleWidgetRender(result: Result<WidgetViewModel, Error>, at index: Int) throws {
        switch result {
            case let .success(widgetData):
                switch widgetData.widget {
                    case .inlineBanner:
                        self.items.insert(widgetData, at: index)

                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                        self.tableView.endUpdates()

                    case .popup:
                        let popupPromo = FDPPopupContainer()
                        try? popupPromo.setup(viewModel: widgetData, screenName: "PopupScreen")
                        self.navigationController?.addPopup(container: popupPromo)

                    case .interstitial:
                        let interstitialPromo = FDPInterstitialContainer()
                        try? interstitialPromo.setup(viewModel: widgetData, screenName: "InterstitialScreen")
                        self.navigationController?.addInterstitial(container: interstitialPromo)

                    case .pinned:
                        let pinnedPromo = FDPPinnedContainer()
                        try? pinnedPromo.setup(viewModel: widgetData, screenName: "PinnedScreen")
                        self.navigationController?.addPinned(container: pinnedPromo)
                        // For tabbar controller
                        // self.tabBarController?.addPinned(container: pinnedPromo, aboveTabBar: true)

                    @unknown default:
                        break
                }
            case let .failure(error):
                print(error)
        }
    }

    private func handleWidgetTypeRender(result: Result<WidgetTypeViewModel, Error>, at index: Int) throws {
        switch result {
            case let .success(widget):
                switch widget {
                    case .inlineBanner:
                        items.insert(widget, at: 5)

                        tableView.beginUpdates()
                        tableView.insertRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
                        tableView.endUpdates()

                    case .popup:
                        let popupPromo = FDPPopupContainer()
                        try? popupPromo.setup(viewModel: widget)
                        self.navigationController?.addPopup(container: popupPromo)

                    case .interstitial:
                        let interstitialPromo = FDPInterstitialContainer()
                        try? interstitialPromo.setup(viewModel: widget)
                        self.navigationController?.addInterstitial(container: interstitialPromo)

                    case .pinned:
                        let pinnedPromo = FDPPinnedContainer()
                        try? pinnedPromo.setup(viewModel: widget)
                        self.navigationController?.addPinned(container: pinnedPromo)
                        // For tabbar controller
                        // self.tabBarController?.addPinned(container: pinnedPromo, aboveTabBar: true)

                    @unknown default:
                        break
                }
            case let .failure(error):
                print(error)
        }
    }
}

extension FDPViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellItem = items[indexPath.row]

        switch cellItem {
            case let textViewModel as String:

                let cell = tableView.dequeueReusableCell(withIdentifier: "DummyDataCell", for: indexPath)
                cell.textLabel?.text = textViewModel
                return cell

            case let widget as WidgetViewModel: // if our item is of type WidgetViewModel

                let cell = tableView.dequeueReusableCell(withIdentifier: "FDPWidgetContainerCell", for: indexPath)

                guard let containerCell = cell as? FDPWidgetTableViewCell else {
                    return cell
                }

                do {
                    try containerCell.set(viewModel: widget, screenName: "WidgetScreen")
                    return containerCell
                } catch {
                    containerCell.textLabel?.text = error.localizedDescription
                    containerCell.textLabel?.numberOfLines = 0
                    print(error)
                    return cell
                }

            case let widgetType as WidgetTypeViewModel:

                let cell = tableView.dequeueReusableCell(withIdentifier: "FDPWidgetContainerCell", for: indexPath)

                guard let containerCell = cell as? FDPWidgetTableViewCell else {
                    return cell
                }
                do {
                    try containerCell.set(viewModel: widgetType)
                    return containerCell
                } catch {
                    containerCell.textLabel?.text = error.localizedDescription
                    containerCell.textLabel?.numberOfLines = 0
                    print(error)
                    return cell
                }

            default:
                fatalError()
        }

    }
}
